﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Net;
using Newtonsoft.Json;
using System.Globalization;

public class LoadingPanel : MonoBehaviour
{
    private string _mainMenu = "Menu";
    private string _webView = "WebView";
    [Space(10)]
    [SerializeField] private GameObject Preloader;
    [SerializeField] private GameObject ConfirmPanel;
    [SerializeField] private Toggle ConfirmAge;
    [SerializeField] private Toggle ConfirmPrivacy;
    [SerializeField] private Button BtnContinue;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(4f);

        if (PlayerPrefs.GetString("ConfirmPrivacy") != null && PlayerPrefs.GetString("ConfirmPrivacy") == "confirmed")
        {
            string country = GetUserCountry();
            if (country != "RU" && country != "KZ" && country != "BE")
                SceneManager.LoadSceneAsync(_mainMenu, LoadSceneMode.Single);
            else
                SceneManager.LoadSceneAsync(_webView, LoadSceneMode.Single);
        }
        else
        {
            Preloader.SetActive(false);
            ConfirmPanel.SetActive(true);

            ConfirmAge.onValueChanged.AddListener(ToggleClick);
            ConfirmPrivacy.onValueChanged.AddListener(ToggleClick);
            BtnContinue.onClick.AddListener(OnContinueClick);
        }
    }

    public void OnContinueClick()
    {
        PlayerPrefs.SetString("ConfirmPrivacy", "confirmed");
        string country = GetUserCountry();
        if (country != "RU" && country != "KZ" && country != "BE")
            SceneManager.LoadSceneAsync(_mainMenu, LoadSceneMode.Single);
        else
            SceneManager.LoadSceneAsync(_webView, LoadSceneMode.Single);
    }

    public void ToggleClick(bool click)
    {
        if(ConfirmAge.isOn && ConfirmPrivacy.isOn)
            BtnContinue.gameObject.SetActive(true);
        else
            BtnContinue.gameObject.SetActive(false);
    }

    public string GetUserCountry(string token = "8443b852df8803")
    {
        IpInfo ipInfo = new IpInfo();
        try
        {
            string info = new WebClient().DownloadString(" http://ipinfo.io/geo?token=" + token);
            ipInfo = JsonConvert.DeserializeObject<IpInfo>(info);
        }
        catch (Exception)
        {
            ipInfo.Country = null;
        }
        return ipInfo.Country;
    }
}

public class IpInfo
{
    [JsonProperty("ip")]
    public string Ip { get; set; }
    [JsonProperty("hostname")]
    public string Hostname { get; set; }
    [JsonProperty("city")]
    public string City { get; set; }
    [JsonProperty("region")]
    public string Region { get; set; }
    [JsonProperty("country")]
    public string Country { get; set; }
    [JsonProperty("loc")]
    public string Loc { get; set; }
    [JsonProperty("org")]
    public string Org { get; set; }
    [JsonProperty("postal")]
    public string Postal { get; set; }
}