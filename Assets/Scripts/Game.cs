﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GoogleMobileAds.Api;

public class Game : MonoBehaviour {
    public enum Statu { Bet, Ready, Wait, Win, Lose, Push};
    public Statu statu;
    public Sprite[] cards, chips;
    public GameObject[] panels;
    public GameObject cardPrefab, checkScore, chipParent;
    public int bet, computerScore, myScore;
    public Text myMoneyText, betText, startInfoText, myScoreText, computerScoreText;
    public SpriteRenderer betSprite;
    public List<GameObject> computerCards, myCards;
    public AudioClip chipSound;
    int myMoney, myExtraCard, computerExtraCard, sortingOrder;
    bool refreshMoney, doubleBet;
    InterstitialAd interstitial;

    void Start() {
        statu = Statu.Bet;
        cards = Resources.LoadAll<Sprite>("Cards");
        myMoneyText.text = "" + PlayerPrefs.GetInt("money");
        OpenPanels(0);
        myMoney = PlayerPrefs.GetInt("money");

        if (Application.internetReachability != NetworkReachability.NotReachable) {
            RequestInterstitial();
        }

        if (myMoney <= 100) {
            if (myMoney >= 5) {
                chipParent.transform.GetChild(0).GetComponent<Button>().interactable = true;
            } else {
                chipParent.transform.GetChild(0).GetComponent<Button>().interactable = false;
            }

            if (myMoney >= 10) {
                chipParent.transform.GetChild(1).GetComponent<Button>().interactable = true;
            } else {
                chipParent.transform.GetChild(1).GetComponent<Button>().interactable = false;
            }

            if (myMoney >= 25) {
                chipParent.transform.GetChild(2).GetComponent<Button>().interactable = true;
            } else {
                chipParent.transform.GetChild(2).GetComponent<Button>().interactable = false;
            }

            if (myMoney >= 50) {
                chipParent.transform.GetChild(3).GetComponent<Button>().interactable = true;
            } else {
                chipParent.transform.GetChild(3).GetComponent<Button>().interactable = false;
            }

            if (myMoney >= 100) {
                chipParent.transform.GetChild(4).GetComponent<Button>().interactable = true;
            } else {
                chipParent.transform.GetChild(4).GetComponent<Button>().interactable = false;
            }
        }
    }

    private void RequestInterstitial() {
        interstitial = new InterstitialAd(Constants.interstitialID);
        AdRequest request = new AdRequest.Builder().Build();
        interstitial.LoadAd(request);
    }

    void Update() {
        if (refreshMoney) {
            if (statu == Game.Statu.Win) {
                if (myMoney < PlayerPrefs.GetInt("money")) {
                    myMoney += 5;
                    myMoneyText.text = "" + myMoney;
                } else {
                    refreshMoney = false;
                    myMoney = PlayerPrefs.GetInt("money");
                    StartCoroutine(RestartGame());
                }
            } else if (statu == Game.Statu.Lose) {
                if (myMoney > PlayerPrefs.GetInt("money")) {
                    myMoney -= 5;
                    myMoneyText.text = "" + myMoney;
                } else {
                    refreshMoney = false;
                    myMoney = PlayerPrefs.GetInt("money");
                    StartCoroutine(RestartGame());
                }
            }
        }
    }

    IEnumerator RestartGame() {
        PlayerPrefs.SetInt("gamePlayed", PlayerPrefs.GetInt("gamePlayed") + 1);
        PlayerPrefs.Save();

        if (PlayerPrefs.GetInt("gamePlayed") >= Constants.gamePlayed) {
            if (interstitial != null) {
                if (interstitial.IsLoaded()) {
                    PlayerPrefs.SetInt("gamePlayed", 0);
                    PlayerPrefs.Save();
                    interstitial.Show();
                } else {
                    RequestInterstitial();
                }
            } else {
                RequestInterstitial();
            }

        }
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Game");
    }

    void OpenPanels(int id) {
        for (int i = 0; i < panels.Length; i++) {
            if (i == id) {
                panels[i].SetActive(true);
            } else {
                panels[i].SetActive(false);
            }
        }
    }

    public void RefreshScores() {
        myScoreText.text = "" + myScore;
        computerScoreText.text = "" + computerScore;
    }

    public void BackBtn() {
        if (bet > 0 && statu != Game.Statu.Bet) {
            PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") - bet);
            PlayerPrefs.Save();
        }
        SceneManager.LoadScene("Menu");
    }

    public void ChipBtn(int count) {
        if (bet < PlayerPrefs.GetInt("money") && statu == Game.Statu.Bet) {
            chipParent.transform.GetChild(count).GetComponent<AudioSource>().PlayOneShot(chipSound);
            if (count == 0) {
                bet += 5;
                betSprite.sprite = chips[0];
                PlayerPrefs.SetInt("lastBetChip", 0);
                PlayerPrefs.Save();
            } else if (count == 1) {
                bet += 10;
                betSprite.sprite = chips[1];
                PlayerPrefs.SetInt("lastBetChip", 1);
                PlayerPrefs.Save();
            } else if (count == 2) {
                bet += 25;
                betSprite.sprite = chips[2];
                PlayerPrefs.SetInt("lastBetChip", 2);
                PlayerPrefs.Save();
            } else if (count == 3) {
                bet += 50;
                betSprite.sprite = chips[3];
                PlayerPrefs.SetInt("lastBetChip", 3);
                PlayerPrefs.Save();
            } else if (count == 4) {
                bet += 100;
                betSprite.sprite = chips[4];
                PlayerPrefs.SetInt("lastBetChip", 4);
                PlayerPrefs.Save();
            }

            betText.text = "" + bet;
        }
    }

    public void ClearBetBtn() {
        betSprite.sprite = null;
        bet = 0;
        betText.text = "" + bet;
    }

    public void LastBetBtn() {
        if (myMoney >= PlayerPrefs.GetInt("lastBet") && myMoney > 0) {
            bet = PlayerPrefs.GetInt("lastBet");
            betText.text = "" + bet;
            if (bet > 0) {
                betSprite.sprite = chips[PlayerPrefs.GetInt("lastBetChip")];
            }
        }
    }

    public void DealBtn() {
        if (bet > 0) {
            startInfoText.text = "";
            statu = Game.Statu.Wait;
            OpenPanels(1);
            StartCoroutine(StartGame());
        }
    }

    IEnumerator StartGame() {
        yield return new WaitForSeconds(0.5f);
        PlayerPrefs.SetInt("lastBet", bet);
        PlayerPrefs.Save();
        computerCards = new List<GameObject>();

        for (int i = 0; i < 4; i++) {
            int whichCard = Random.Range(2, cards.Length);
            GameObject currentCard = (GameObject)Instantiate(cardPrefab, new Vector2(3.8f, 1.6f), Quaternion.identity);
            currentCard.GetComponent<SpriteRenderer>().sprite = cards[whichCard];
            currentCard.GetComponent<Card>().original = currentCard.GetComponent<SpriteRenderer>().sprite;
            currentCard.name = currentCard.GetComponent<SpriteRenderer>().sprite.name;
            currentCard.GetComponent<SpriteRenderer>().sortingOrder = sortingOrder;

            if (i == 0) {
                currentCard.GetComponent<Card>().newPos = new Vector2(-0.35f, -0.2f);
                myCards.Add(currentCard);
            } else if (i == 1) {
                currentCard.GetComponent<Card>().newPos = new Vector2(-0.35f, 3.25f);
                computerCards.Add(currentCard);
            } else if (i == 2) {
                currentCard.GetComponent<Card>().newPos = new Vector2(0.35f, -0.2f);
                myCards.Add(currentCard);
            } else if (i == 3) {
                currentCard.GetComponent<Card>().newPos = new Vector2(0.35f, 3.25f);
                computerCards.Add(currentCard);
            }
            sortingOrder++;
            yield return new WaitForSeconds(0.7f);
        }

        statu = Game.Statu.Ready;
    }

    public void DoubleBtn() {
        if (statu == Game.Statu.Ready) {
            int doubleBetPrice = bet + bet;
            if (PlayerPrefs.GetInt("money") >= doubleBetPrice) {
                bet += bet;
                betText.text = "" + bet;
                panels[1].transform.GetChild(0).GetComponent<Button>().interactable = false;
                myExtraCard = 0;
                doubleBet = true;
            }
        }
    }

    public void StayBtn() {
        if (statu == Game.Statu.Ready) {
            statu = Game.Statu.Wait;
            computerCards[1].GetComponent<Card>().OpenCard();
        }
    }

    public void HitBtn() {
        if (statu == Game.Statu.Ready) {
            if (myExtraCard < 1) {
                myExtraCard = 1;
                StartCoroutine(GetNewCard(0));
            }
        }
    }

    IEnumerator GetNewCard(int id) {
        statu = Game.Statu.Wait;
        yield return new WaitForSeconds(0.25f);

        GameObject currentCard = (GameObject)Instantiate(cardPrefab, new Vector2(3.8f, 1.6f), Quaternion.identity);
        currentCard.GetComponent<SpriteRenderer>().sprite = cards[Random.Range(2, cards.Length)];
        currentCard.GetComponent<Card>().original = currentCard.GetComponent<SpriteRenderer>().sprite;
        currentCard.name = currentCard.GetComponent<SpriteRenderer>().sprite.name;
        currentCard.GetComponent<SpriteRenderer>().sortingOrder = sortingOrder;
        sortingOrder++;

        if (id == 0) {
            currentCard.GetComponent<Card>().newPos = new Vector2(myCards[myCards.Count - 1].transform.position.x + 0.7f, -0.2f);
            yield return new WaitForSeconds(0.7f);

            myCards.Add(currentCard);
            if (myScore >= 21) {
                computerCards[1].GetComponent<Card>().OpenCard();
            } else {
                statu = Game.Statu.Ready;
            }
        } else {
            currentCard.GetComponent<Card>().newPos = new Vector2(computerCards[computerCards.Count - 1].transform.position.x + 0.7f, 3.25f);
            yield return new WaitForSeconds(0.7f);

            computerCards.Add(currentCard);
            computerScore += currentCard.GetComponent<Card>().score;
            computerScoreText.text = "" + computerScore;

            if (computerScore > 21) {
                Win();
            } else if (computerScore == 21) {
                Blackjack(1);
            } else {
                CheckScores();
            }
        }
    }

    public void CardOpened() {
        if (computerScore < 17) {
            if (computerScore > myScore) {
                CheckScores();
            } else {
                if (myScore > 21 || computerScore > myScore) {
                    CheckScores();
                } else {
                    if (computerExtraCard < 1) {
                        computerExtraCard = 1;
                        StartCoroutine(GetNewCard(1));
                    }
                }
            }
        } else {
            CheckScores();
        }
    }

    public void CheckScores() {
        if (myScore == 21) {
            Blackjack(0);
        } else if (computerScore == 21) {
            Blackjack(1);
        } else if (myScore == 21 && computerScore == 21) {
            Blackjack(2);
        } else {
            if (myScore > computerScore) {
                if (myScore <= 21) {
                    Win();
                } else {
                    Lose();
                }
            } else if (myScore < computerScore) {
                if (computerScore <= 21) {
                    Lose();
                } else {
                    Win();
                }
            } else if (myScore == computerScore) {
                statu = Game.Statu.Push;
                checkScore.transform.GetChild(3).GetComponent<Image>().enabled = true;
                StartCoroutine(RestartGame());
            }
        }
    }

    void Win() {
        statu = Game.Statu.Win;
        checkScore.transform.GetChild(0).GetComponent<Image>().enabled = true;
        PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + bet);
        PlayerPrefs.Save();
        refreshMoney = true;
    }

    void Lose() {
        statu = Game.Statu.Lose;
        checkScore.transform.GetChild(1).GetComponent<Image>().enabled = true;
        PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") - bet);
        PlayerPrefs.Save();
        refreshMoney = true;
    }

    void Blackjack(int id) {
        if (id == 0) {
            statu = Game.Statu.Win;
            checkScore.transform.GetChild(2).GetComponent<Image>().enabled = true;
            PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + bet);
            PlayerPrefs.Save();
            refreshMoney = true;
        } else if (id == 1) {
            statu = Game.Statu.Lose;
            checkScore.transform.GetChild(2).GetComponent<Image>().enabled = true;
            PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") - bet);
            PlayerPrefs.Save();
            refreshMoney = true;
        } else if (id == 2) {
            StartCoroutine(RestartGame());
        }
    }
}
