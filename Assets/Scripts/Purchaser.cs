﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class Purchaser : MonoBehaviour {//, IStoreListener {
    //private static IStoreController m_StoreController;
    //private static IExtensionProvider m_StoreExtensionProvider;
    //public static string kProductIDConsumable1 = "m1000";
    //public static string kProductIDConsumable2 = "m5000";
    //public static string kProductIDConsumable3 = "m10000";
    //public static string kProductIDConsumable4 = "m25000";

    //void Start() {
    //    if (m_StoreController == null) {
    //        InitializePurchasing();
    //    }
    //}

    //public void InitializePurchasing() {
    //    if (IsInitialized()) {
    //        return;
    //    }

    //    var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
    //    builder.AddProduct(Constants.purchaseMoney1, ProductType.Consumable);
    //    builder.AddProduct(Constants.purchaseMoney2, ProductType.Consumable);
    //    builder.AddProduct(Constants.purchaseMoney3, ProductType.Consumable);
    //    builder.AddProduct(Constants.purchaseMoney4, ProductType.Consumable);

    //    UnityPurchasing.Initialize(this, builder);
    //}


    //private bool IsInitialized() {
    //    return m_StoreController != null && m_StoreExtensionProvider != null;
    //}


    //public void BuyConsumable(int id) {
    //    if (id == 0) {
    //        BuyProductID(Constants.purchaseMoney1);
    //    } else if (id == 1) {
    //        BuyProductID(Constants.purchaseMoney2);
    //    } else if (id == 2) {
    //        BuyProductID(Constants.purchaseMoney3);
    //    } else if (id == 3) {
    //        BuyProductID(Constants.purchaseMoney4);
    //    }
    //}

    //void BuyProductID(string productId) {
    //    if (IsInitialized()) {
    //        Product product = m_StoreController.products.WithID(productId);
    //        if (product != null && product.availableToPurchase) {
    //            Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
    //            m_StoreController.InitiatePurchase(product);
    //        } else {
    //            Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
    //        }
    //    } else {
    //        Debug.Log("BuyProductID FAIL. Not initialized.");
    //    }
    //}

    //public void RestorePurchases() {
    //    if (!IsInitialized()) {
    //        Debug.Log("RestorePurchases FAIL. Not initialized.");
    //        return;
    //    }

    //    if (Application.platform == RuntimePlatform.IPhonePlayer ||
    //        Application.platform == RuntimePlatform.OSXPlayer) {
    //        Debug.Log("RestorePurchases started ...");

    //        var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
    //        apple.RestoreTransactions((result) => {
    //            Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
    //        });
    //    } else {
    //        Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
    //    }
    //}

    //public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
    //    Debug.Log("OnInitialized: PASS");
    //    m_StoreController = controller;
    //    m_StoreExtensionProvider = extensions;
    //}


    //public void OnInitializeFailed(InitializationFailureReason error) {
    //    Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    //}


    //public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) {
    //    if (string.Equals(args.purchasedProduct.definition.id, Constants.purchaseMoney1, System.StringComparison.Ordinal)) {
    //        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
    //        PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + 1000);
    //        PlayerPrefs.Save();
    //    } else if (string.Equals(args.purchasedProduct.definition.id, Constants.purchaseMoney2, System.StringComparison.Ordinal)) {
    //        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
    //        PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + 5000);
    //        PlayerPrefs.Save();
    //    } else if (string.Equals(args.purchasedProduct.definition.id, Constants.purchaseMoney3, System.StringComparison.Ordinal)) {
    //        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
    //        PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + 10000);
    //        PlayerPrefs.Save();
    //    } else if (string.Equals(args.purchasedProduct.definition.id, Constants.purchaseMoney4, System.StringComparison.Ordinal)) {
    //        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
    //        PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + 25000);
    //        PlayerPrefs.Save();
    //    } else {
    //        Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
    //    }
    //    return PurchaseProcessingResult.Complete;
    //}


    //public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason) {
    //    Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    //}
}
