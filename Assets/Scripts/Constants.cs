﻿

public static class Constants {

    //Admob ids
    public static string interstitialID = "";
    public static string rewardedVideoID = "";

    //Show interstitial ad every 3 game played.
    public static int gamePlayed = 3;

    //Give award after rewarded video watched
    public static int videoAdRewardMoney = 250;
    //Start money
    public static int startMoney = 250;

    //1000 Money ID for In-App Purchase
    public static string purchaseMoney1 = "money1000";
    //5000 Money ID for In-App Purchase
    public static string purchaseMoney2 = "money5000";
    //10000 Money ID for In-App Purchase
    public static string purchaseMoney3 = "money10000";
    //25000 Money ID for In-App Purchase
    public static string purchaseMoney4 = "money25000";
}
