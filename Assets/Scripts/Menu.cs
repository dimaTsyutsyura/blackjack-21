﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;

public class Menu : MonoBehaviour {
    public GameObject[] screens;
    RewardBasedVideoAd rewardedAd;
    bool setReward;

    // Use this for initialization
    void Start() {
        setReward = false;
        if (PlayerPrefs.GetInt("firstTime") == 0) {
            PlayerPrefs.SetInt("money", Constants.startMoney);
            PlayerPrefs.SetInt("firstTime", 1);
            PlayerPrefs.Save();
        }
        OpenScreen(0);
        if (Application.internetReachability != NetworkReachability.NotReachable) {
            RequestRewardVideo();
        }
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
    }

    void RequestRewardVideo() {
        rewardedAd = RewardBasedVideoAd.Instance;
        AdRequest request = new AdRequest.Builder().AddKeyword("game").TagForChildDirectedTreatment(false).Build();
        if (!setReward) {
            rewardedAd.OnAdRewarded += HandleRewardBasedVideoRewarded;
            setReward = true;
        }
        rewardedAd.LoadAd(request, Constants.rewardedVideoID);
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args) {
        PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money") + Constants.videoAdRewardMoney);
        PlayerPrefs.Save();
    }

    void OpenScreen(int id) {
        for (int i = 0; i < screens.Length; i++) {
            if (i == id) {
                screens[i].SetActive(true);
            } else {
                screens[i].SetActive(false);
            }
        }
    }

    public void PlayBtn() {
        SceneManager.LoadScene("Game");
    }

    public void RateBtn() {
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
    }

    public void ShopBtn() {
        OpenScreen(1);
    }

    public void MainMenuBtn() {
        OpenScreen(0);
    }

    public void ShowVideoAd() {
        if (Application.internetReachability != NetworkReachability.NotReachable) {
            if (rewardedAd != null) {
                if (rewardedAd.IsLoaded()) {
                    rewardedAd.Show();
                } else {
                    RequestRewardVideo();
                }
            } else {
                RequestRewardVideo();
            }
        }
    }

    public void ExitBtn() {
        Application.Quit();
    }
}
