﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour {
    public Vector2 newPos;
    public AudioClip cardSound;
    public Sprite backB, backR, original;
    public bool move, openCard;
    public int score;
    SpriteRenderer sRenderer;

    // Use this for initialization
    void OnEnable() {
        move = true;
        openCard = false;
        newPos = transform.position;
    }

    void Start() {
        sRenderer = GetComponent<SpriteRenderer>();

        string myName = transform.name;
        string[] myNameArray = myName.Split('-');
        if (myNameArray[1] == "J" || myNameArray[1] == "Q" || myNameArray[1] == "K") {
            score = 10;
        } else {
            int convertName = int.Parse(myNameArray[1]);
            score = convertName;
        }

        if (newPos.x == 0.35f && newPos.y == 3.25f) {
            if (myNameArray[1] == "J" || myNameArray[1] == "Q" || myNameArray[1] == "K") {
                if (Camera.main.GetComponent<Game>().computerCards[0].GetComponent<Card>().score == 1) {
                    score = 11;
                } else {
                    score = 10;
                }
            } else if (score == 1) {
                string firstCardName = Camera.main.GetComponent<Game>().computerCards[0].name;
                string[] firstCardNameArray = firstCardName.Split('-');
                if (firstCardNameArray[1] == "J" || firstCardNameArray[1] == "Q" || firstCardNameArray[1] == "K") {
                    score = 11;
                }
            } else {
                int convertName = int.Parse(myNameArray[1]);
                score = convertName;
            }

            if (transform.name.StartsWith("b")) {
                sRenderer.sprite = backB;
            } else if (transform.name.StartsWith("r")) {
                sRenderer.sprite = backR;
            }
        }

        if (newPos.x == 0.35f && newPos.y == -0.2f) {
            if (myNameArray[1] == "J" || myNameArray[1] == "Q" || myNameArray[1] == "K") {
                if (Camera.main.GetComponent<Game>().myCards[0].GetComponent<Card>().score == 1) {
                    score = 11;
                } else {
                    score = 10;
                }
            } else if (score == 1) {
                string firstCardName = Camera.main.GetComponent<Game>().myCards[0].name;
                string[] firstCardNameArray = firstCardName.Split('-');
                if (firstCardNameArray[1] == "J" || firstCardNameArray[1] == "Q" || firstCardNameArray[1] == "K") {
                    score = 11;
                }
            } else {
                int convertName = int.Parse(myNameArray[1]);
                score = convertName;
            }
        }

        GetComponent<AudioSource>().PlayOneShot(cardSound);
    }

    // Update is called once per frame
    void Update() {
        if (move) {
            transform.position = Vector2.MoveTowards((Vector2)transform.position, newPos, 20 * Time.deltaTime);
            if (transform.position.x == newPos.x && transform.position.y == newPos.y) {
                if (newPos.y == -0.2f) {
                    Camera.main.GetComponent<Game>().myScore += score;
                    Camera.main.GetComponent<Game>().RefreshScores();
                } else {
                    if (newPos.x == -0.35f) {
                        Camera.main.GetComponent<Game>().computerScore += score;
                        Camera.main.GetComponent<Game>().RefreshScores();
                    }
                }
                move = false;
            }
        }

        if (openCard) {
            if (sRenderer.sprite == backB || sRenderer.sprite == backR) {
                transform.localScale = Vector2.MoveTowards(transform.localScale, new Vector2(0, 0.6f), 5 * Time.deltaTime);
                if (transform.localScale.x == 0) {
                    sRenderer.sprite = original;
                }
            } else {
                transform.localScale = Vector2.MoveTowards(transform.localScale, new Vector2(0.6f, 0.6f), 5 * Time.deltaTime);
                if (transform.localScale.x == 0.6f) {
                    Camera.main.GetComponent<Game>().computerScore += score;
                    Camera.main.GetComponent<Game>().RefreshScores();
                    Camera.main.GetComponent<Game>().CardOpened();
                    openCard = false;
                }
            }
        }
    }

    public void OpenCard() {
        openCard = true;
    }
}
